import Vue from 'vue';
import VueRouter from 'vue-router';

import IndexGalleries from './../pages/IndexGalleries.vue';
import AuthorGalleries from './../pages/AuthorGalleries.vue';
import ShowGallery from './../pages/ShowGallery.vue';
import CreateGallery from './../pages/CreateGallery.vue';
import Login from './../pages/Login.vue';
import Register from './../pages/Register.vue';

import { requiresAuth, guestOnly } from './guards';

Vue.use(VueRouter)

const routes = [
    //{ path: '/', redirect: '/all-galleries', name: 'brand' },
    { path: '/login', component: Login, name: 'login', meta: { guestOnly: true } },
    { path: '/register', component: Register, name: 'register', meta: { guestOnly: true } },
    { path: '/', component: IndexGalleries, name: 'all-galleries' },
    { path: '/galleries/:id', component: ShowGallery, name: 'show-gallery' },
    { path: '/authors/:id', component: AuthorGalleries, name: 'author-galleries' },
    { path: '/my-galleries', component: AuthorGalleries, name: 'my-galleries' },
    { path: '/create', component: CreateGallery, name: 'create-gallery', meta: { requiresAuth: true } },
    { path: '/edit-gallery/:id', component: CreateGallery, name: 'edit-gallery', meta: { requiresAuth: true } }
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    Promise.resolve(to).then(requiresAuth).then(guestOnly).then(() => {
        next();
    }).catch(redirect => {
        next(redirect);
    })
});
export default router;