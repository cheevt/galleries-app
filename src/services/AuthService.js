import axios from 'axios';

export default class AuthService {
    constructor() {
        axios.defaults.baseURL = 'http://localhost:8000/api/';
    }
    login(email, password) {
        return axios.post('login', { email, password }).then((response) => {
            window.localStorage.setItem('loginToken', response.data.token);
            window.localStorage.setItem('loggedUserId', response.data.user_id);
            this.setAxiosDefaultAuthorizationHeader();
        });
    }
    setAxiosDefaultAuthorizationHeader() {
        const TOKEN = 'Bearer ' + window.localStorage.getItem('loginToken');
        axios.defaults.headers.common['Authorization'] = TOKEN;
    }
    isAuthenticated() {
        return !!window.localStorage.getItem('loginToken');
    }
    logout() {
        window.localStorage.removeItem('loginToken');
        window.localStorage.removeItem('loggedUserId');
        delete axios.defaults.headers.common['Authorization'];
    }
    register(user) {
        //return axios.post('register', user);
        return axios.post('register', user).then((response) => {
            window.localStorage.setItem('loginToken', response.data.token);
            window.localStorage.setItem('loggedUserId', response.data.user_id);
            this.setAxiosDefaultAuthorizationHeader();
        });
    }
    getAuthor(userId, searchTerm = '') {
        return axios.get(`authors/${userId}`, { params: { searchTerm } });
    }
}
export const authService = new AuthService();