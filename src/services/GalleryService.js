import axios from 'axios';

export default class GalleryService {
    constructor() {
        axios.defaults.baseURL = 'http://127.0.0.1:8000/api/';
    }
    getAll(searchTerm = '') {
        return axios.get('galleries', { params: { searchTerm } });
    }
    show(id) {
        return axios.get(`galleries/${id}`);
    }
    add(gallery) {
        this.setAxiosDefaultAuthorizationHeader();
        return axios.post('galleries', gallery);
    }
    update(gallery) {
        this.setAxiosDefaultAuthorizationHeader();
        return axios.put(`galleries/${gallery.id}`, gallery);
    }
    delete(id) {
        this.setAxiosDefaultAuthorizationHeader();
        return axios.delete(`galleries/${id}`);
    }
    setAxiosDefaultAuthorizationHeader() {
        const TOKEN = 'Bearer ' + window.localStorage.getItem('loginToken');
        axios.defaults.headers.common['Authorization'] = TOKEN;
    }
}

export const galleryService = new GalleryService()