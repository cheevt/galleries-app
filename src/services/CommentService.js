import axios from 'axios';

export default class CommentService {
    constructor() {
        axios.defaults.baseURL = 'http://127.0.0.1:8000/api/';
    }
    add(comment) {
        this.setAxiosDefaultAuthorizationHeader();
        return axios.post('comments', comment);
    }
    delete(id) {
        this.setAxiosDefaultAuthorizationHeader();
        return axios.delete(`comments/${id}`);
    }
    setAxiosDefaultAuthorizationHeader() {
        const TOKEN = 'Bearer ' + window.localStorage.getItem('loginToken');
        axios.defaults.headers.common['Authorization'] = TOKEN;
    }
}

export const commentService = new CommentService()