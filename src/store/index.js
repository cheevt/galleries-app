import Vue from 'vue';
import Vuex from 'vuex';

import { galleryService } from './../services/GalleryService';
import { authService } from './../services/AuthService';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        isAuthenticated: false,
        searchTerm: '',
        userId: '',
        loggedUserId: '',
        galleries: []
    },
    getters: {
        getIsAuthenticated(state) {
            return state.isAuthenticated;
        },
        getLoggedUserId(state) {
            return state.loggedUserId;
        },
        getSearchTerm(state) {
            return state.searchTerm;
        },
        getGalleries(state) {
            return state.galleries;
        }
    },
    mutations: {
        setIsAuthenticated(state, auth) {
            state.isAuthenticated = auth;
        },
        setSearchTerm(state, searchTerm) {
            state.searchTerm = searchTerm;
        },
        setLoggedUserId(state, loggedUserId) {
            state.loggedUserId = loggedUserId;
        },
        setUserId(state, userId) {
            state.userId = userId;
        },
        setGalleries(state, galleries) {
            state.galleries = galleries;
        }
    },
    actions: {
        fetchGalleries({ commit, state }, payload) {
            galleryService.getAll(state.searchTerm).then((response) => {
                let galleries = response.data;
                //console.log(galleries);
                store.commit('setGalleries', galleries);
            })
        },
        fetchAuthorGalleries({ commit, state }, payload) {
            authService.getAuthor(state.userId, state.searchTerm).then((response) => {
                let galleries = response.data;
                //console.log(galleries);
                store.commit('setGalleries', galleries);
            })
        }
    }
})